all: main.o lib.o dict.o
	ld main.o lib.o dict.o -o lab
%.o: %.asm
	nasm -felf64 -o $@ $<
clean:
	rm -rf *.o lab