%define STDOUT 1
%define STDERR 2
%include 'colon.inc'
%include 'words.inc'

global _start

extern find_word
extern read_word
extern print_string
extern print_newline
extern string_length
extern exit

section .data
BUFFER: times 256 db 0
input_msg: db 'Key: ', 0
find_msg: db 'Found successfully: ', 0
not_find_msg: db 'Nothing found.', 0
read_error: db 'Error. Unknow word', 0

section .text
_start:
	mov rdi, input_msg
    	mov rsi, STDOUT
    	call print_string
	mov rdi, BUFFER
    	call read_word
    	test rax, rax
    	jz .err_read
    	mov rsi, current
    	mov rdi, rax
    	call find_word
    	test rax, rax
    	je .not_find
   	add rax, 8
    	mov rsi, rax
    	call string_length
    	add rax, rsi
    	inc rax
	push rax
	mov rdi, find_msg
	mov rsi, STDOUT 
	call print_string
	pop rdi
	mov rsi, STDOUT
	jmp .end

.err_read:
	mov rdi, read_error
	mov rsi, STDERR
	jmp .end

.not_find:
	mov rdi, not_find_msg
	mov rsi, STDERR
	jmp .end
.end:
	call print_string
	call print_newline
	call exit
