section .text
global find_word

extern string_equals

find_word:
.loop:
	cmp rsi, 0
	je .end
	push rsi
    push rdi
	add rsi, 8
	call string_equals
	pop rdi
    pop rsi
	cmp rax, 0
	jne .is_find
	mov rsi, [rsi]
	cmp rsi, 0
	jne .loop
.end:
	xor rax, rax
    ret
.is_find:
    mov rax, rsi
    ret
