global print_newline
global print_string
global exit
global read_word
global string_equals
global string_length

; Принимает код возврата и завершает текущий процесс
exit:
	mov rax, 60
	syscall
	ret

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	xor rax, rax
.loop:
	cmp byte[rdi+rax], 0
	je .end
	inc rax
	jmp .loop
.end:
	ret

; Принимает указатель на нуль-терминированную строку и id потока вывода, выводит её в выбранный поток
print_string:
	xor rax, rax
	mov rsi, rdi
	call string_length
	mov rdx, rax
	mov rdi, 1
	mov rax, 1
	syscall
	ret

; Принимает код символа и выводит его в stdout
print_char:
	push rdi
	mov rdi, rsp
	call print_string
	pop rdi
	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	xor rax, rax
	mov rdi, 0xA
	call print_char
	ret

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    	mov r8, 10
    	mov rax, rdi
    	mov rdi, rsp
    	dec rdi
    	push 0
    	sub rsp, 16
.loop:
    	xor rdx, rdx
    	div r8
    	add rdx, '0'
    	dec rdi
    	mov byte[rdi], dl
    	test rax, rax
    	jnz .loop
    	call print_string
    	add rsp, 24
    	ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
	cmp rdi, 0
	jnl .positive
    	push rdi
	mov rdi, '-'
	call print_char
	pop rdi
	neg rdi
.positive:
	call print_uint
	ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	xor rax, rax
	xor rcx, rcx
.loop:
	mov r8b, byte [rdi + rcx]
	mov r9b, byte [rsi + rcx]
	inc rcx
	cmp r8b, r9b
	jne .not_equal
	cmp r8b, 0
	jne .loop
	mov rax, 1
	ret
.not_equal:
	mov rax, 0
	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	xor rdi, rdi
	xor rax, rax
	mov rdx, 1
	push 0
	mov rsi, rsp
	syscall
	pop rax
	ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	xor r9, r9
	mov r10, rdi
.loop:
	push rdi
	push rsi
	call read_char
	pop rsi
	pop rdi
	cmp rax, 0x20
	jz .if_null
	cmp rax, 0x9
	jz .if_null
	cmp rax, 0xA
	jz .if_null
	cmp r9, rsi
	jae .error
	cmp rax, 0
	jz .end
	mov [rdi + r9], rax
	inc r9
	jmp .loop
.if_null:
	cmp r9, 0
	jnz .end
	jmp .loop
.error:
	xor rax, rax
	ret
.end:
	mov rax, r10
	xor r10, r10
	mov [rdi + r9], r10
	mov rdx, r9
	xor r9, r9
	ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	xor rax, rax
    	xor rdx, rdx
	xor r9, r9
.loop:    
    	mov r9b, byte[rdi + rdx]
    	cmp r9b, '0'
    	jb .end
    	cmp r9b, '9'
    	ja .end
    	imul rax, 10
    	sub r9b, '0'
    	add rax, r9
    	inc rdx
    	jmp .loop
.end:
    	ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    	mov r9b, byte[rdi]
    	cmp r9b, '-'
	push r9
    	jne .parse
    	inc rdi
.parse:
    	call parse_uint
    	pop r9
    	cmp r9b, '-'
    	jne .positive
    	neg rax
    	inc rdx
.positive:
    	ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	xor rax, rax
	call string_length
	inc rax
	cmp rax, rdx
	ja .error
	xor rcx, rcx
.loop:
	mov rdx, [rdi + rcx]    
    	mov [rsi + rcx], rdx   
    	inc rcx                 
    	cmp rax, rcx
    	jne .loop
    	ret
.error:
	xor rax, rax
	ret