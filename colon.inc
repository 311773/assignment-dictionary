%define current 0
%macro colon 2
	%ifid %2
		%2: dq current
		%define current %2
	%else
		%fatal "2 Error"
	%endif	
	%ifstr %1
		db %1, 0
	%else
		%fatal "1 Error"
	%endif	
%endmacro